function spalanie()
    {
		var zmienna1=document.getElementById("pole1").value;//wczytanie watości zmiennej z pola formularza
		var zmienna2=document.getElementById("pole2").value;//wczytanie watości zmiennej z pola formularza
		var zmienna3=document.getElementById("pole3").value;//wczytanie watości zmiennej z pola formularza
		var wynik=0;//stworzenie zmiennej pomocniczej i przypisanie wartości 0
		var koszt=0;//stworzenie zmiennej pomocniczej i przypisanie wartości 0
       
		wynik=(Number(zmienna2))*100/Number(zmienna1);//obliczenia dla spalania
		koszt=Number(zmienna3)*Number(zmienna2);//obliczenia dla kosztów trasy
		
		if(zmienna3>0) {//sprawdzenie czy wyświetlamy samo spalanie, czy również koszty trasy
		document.getElementById("cenaspal").innerHTML="Spalanie (l/100km): "+koszt;//wyświetlanie wyniku
		document.getElementById("wynikspal").innerHTML="Koszt za trasę (w PLN): "+wynik;//wyświetlanie wyniku
		}
		else document.getElementById("wynikspal").innerHTML="Koszt za trasę (w PLN): "+wynik;//wyświetlanie wyniku
    }
	
function oplacalnosc()
	{
		var cenabenz=document.getElementById("cenabenzyny").value;//wczytanie watości zmiennej z pola formularza
		var cenagaz=document.getElementById("cenagazu").value;//wczytanie watości zmiennej z pola formularza
		var spalaniebenz=document.getElementById("spalaniebenzyny").value;//wczytanie watości zmiennej z pola formularza
		var msprzebieg=document.getElementById("miesiecznyprzebieg").value;//wczytanie watości zmiennej z pola formularza
		var okresekspl=document.getElementById("okreseksploatacji").value;//wczytanie watości zmiennej z pola formularza
		
		if(cenabenz<1 || cenagaz<1 || spalaniebenz<1 || msprzebieg<1 || okresekspl<1) alert("Podaj poprawne dane!");//sprawdzenie danych i ewentualne wyświetlanie komunikatu o konieczności poprawienia danych
		
		var spalaniegaz=Number(spalaniebenz)*1.10;//stworzenie zmiennej pomocniczej, wczytanie oraz konwersja na liczbę, mnożenie - obliczenie spalania dla gazu
		var rocznyprzebieg=msprzebieg*12;//stworzenie zmiennej pomocniczej i obliczenie rocznego przebiegu
		var przebieg=(rocznyprzebieg*Number(okresekspl));//stworzenie zmiennej pomocniczej i obliczenie przebiegu w okresie eksploatacji
		var kosztybenz=((przebieg)/100)*Number(spalaniebenz)*Number(cenabenz);//stworzenie zmiennej pomocniczej i obliczenie kosztów benzyny
		var kosztygaz=((przebieg)/100)*Number(spalaniegaz)*Number(cenagaz);//stworzenie zmiennej pomocniczej i obliczenie kosztów gazu
		var oszczędność=kosztybenz-kosztygaz;//stworzenie zmiennej pomocniczej i obliczenie różnicy między kosztami benzyny, a kosztami gazu

		if(cenabenz>0 && cenagaz>0 && spalaniebenz>0 && msprzebieg>0 && okresekspl>0)//sprawdzenie poprawności danych
		{
		if(kosztygaz<kosztybenz) {//sprawdzenie, a następnie wyświetlenie odpowiedniego wyniku dla większych kosztów benzyny od kosztów gazu
		document.getElementById("oplacalnosc").innerHTML="Montaż instalacji jest opłacalny. <br> Twój miesięczny przebieg wynosi "+msprzebieg+" kilometrów, w ciągu roku przejedziesz natomiast " +rocznyprzebieg+" kilometrów, a w przewidywanym okresie użytkowania auta "+przebieg+" kilometrów. <br> Koszty benzyny dla Twojego pojazdu w podanym okresie czasu ("+okresekspl+" lat) to "+kosztybenz+" złotych. Natomiast koszty gazu dla Twojego pojazdu w podanym okresie czasu ("+okresekspl+" lat) to "+kosztygaz+" złotych. Pozwala to zaoszczędzić "+oszczędność+" złotych. "+'<br>'+'<br>'+"Dobrze to wygląda? Zachęcamy do skontaktowania się z nami poprzez zakładkę kontakt w celu dokładnej wyceny montażu instalacji w Twoim samochodzie!"
		}
		else if(kosztygaz==kosztybenz) {//sprawdzenie, a następnie wyświetlenie odpowiedniego wyniku dla równych kosztów
		document.getElementById("oplacalnosc").innerHTML="Bez różnicy. Montaż instalacji w podanym czasie nie przyniesie ani korzyści, ani strat.<br> Twój miesięczny przebieg wynosi "+msprzebieg+" kilometrów, w ciągu roku przejedziesz natomiast " +rocznyprzebieg+" kilometrów, a w przewidywanym okresie użytkowania auta "+przebieg+" kilometrów. <br> Koszty benzyny dla Twojego pojazdu w podanym okresie czasu ("+okresekspl+" lat) to "+kosztybenz+" złotych. Natomiast koszty gazu dla Twojego pojazdu w podanym okresie czasu ("+okresekspl+" lat) to "+kosztygaz+" złotych.";
		}
		else {//wyświetlenie odpowiedniego wyniku dla sytuacji gdy koszty gazu są większe od kosztów benzyny
		document.getElementById("oplacalnosc").innerHTML="Nieopłacalne. Montaż instalacji w podanym czasie narazi Cię na straty finansowe.<br> Twój miesięczny przebieg wynosi "+msprzebieg+" kilometrów, w ciągu roku przejedziesz natomiast " +rocznyprzebieg+" kilometrów, a w przewidywanym okresie użytkowania auta "+przebieg+" kilometrów. <br> Koszty benzyny dla Twojego pojazdu w podanym okresie czasu ("+okresekspl+" lat) to "+kosztybenz+" złotych. Natomiast koszty gazu dla Twojego pojazdu w podanym okresie czasu ("+okresekspl+" lat) to "+kosztygaz+" złotych. Oznacza to stratę "+(-1*oszczędność)+" złotych. ";
		}
		}
	}